# ubuntu-20.04-base
# Copyright (C) 2020-2021 Intel Corporation
#
# SPDX-License-Identifier: GPL-2.0-only
#

FROM ubuntu:20.04 AS yocto-sdk
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y \
        gawk \
        wget \
        git-core \
        subversion \
        diffstat \
        unzip \
        sysstat \
        texinfo \
        build-essential \
        chrpath \
        socat \
        python \
        python3 \
        python3-pip \
        python3-pexpect \
        xz-utils  \
        locales \
        cpio \
        screen \
        tmux \
        sudo \
        iputils-ping \
        python3-git \
        python3-jinja2 \
        libegl1-mesa \
        libsdl1.2-dev \
        pylint3 \
        xterm \
        iproute2 \
        fluxbox \
        tightvncserver \
        lz4 \
	gcc-multilib g++-multilib \
        zstd && \
    cp -af /etc/skel/ /etc/vncskel/ && \
    echo "export DISPLAY=1" >>/etc/vncskel/.bashrc && \
    mkdir  /etc/vncskel/.vnc && \
    echo "" | vncpasswd -f > /etc/vncskel/.vnc/passwd && \
    chmod 0600 /etc/vncskel/.vnc/passwd && \
    useradd -U -m yoctouser && \
    /usr/sbin/locale-gen en_US.UTF-8 && \
    echo 'dash dash/sh boolean false' | debconf-set-selections && \
    dpkg-reconfigure dash

COPY build-install-dumb-init.sh /
RUN  bash /build-install-dumb-init.sh && \
     rm /build-install-dumb-init.sh && \
     apt-get clean

USER yoctouser
WORKDIR /home/yoctouser
CMD /bin/bash

FROM yocto-sdk AS arago-sdk

RUN echo . \
	&& mkdir -p /opt \
	&& wget https://developer.arm.com/-/media/Files/downloads/gnu-a/9.2-2019.12/binrel/gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf.tar.xz \
	&& tar xf gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf.tar.xz \
	&& rm gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf.tar.xz \
	&& wget https://developer.arm.com/-/media/Files/downloads/gnu-a/9.2-2019.12/binrel/gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu.tar.xz \
	&& tar xf gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu.tar.xz \
	&& rm gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu.tar.xz \
	&& echo .

ENV TOOLCHAIN_BASE_ARMV7=/home/yoctouser/gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf
ENV TOOLCHAIN_BASE_ARMV8=/home/yoctouser/gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu
ENV MACHINE=j7-evm

ENV PSDK_VERSION=08_01_00_07
WORKDIR /home/yoctouser/yocto-build
RUN echo . \
	&& git clone http://arago-project.org/git/projects/oe-layersetup.git \
	&& cd oe-layersetup \
	&& ./oe-layertool-setup.sh -f configs/processor-sdk-linux/processor-sdk-linux-08_01_00.txt \
	&& cd build \
	&& echo "INHERIT += \"own-mirrors\"" >> conf/local.conf \
	&& echo "SOURCE_MIRROR_URL = \"https://software-dl.ti.com/processor-sdk-mirror/sources/\"" >> conf/local.conf \
	&& echo "ARAGO_BRAND  = \"psdkla\"" >> conf/local.conf \
	&& echo "DISTRO_FEATURES_append = \" virtualization\"" >> conf/local.conf \
	&& . conf/setenv \
	&& echo .

	#&& MACHINE=j7-evm bitbake -k arago-core-psdkla-bundle \

FROM ubuntu:20.04 AS ubuntu-tisdk1
ENV DEBIAN_FRONTEND=noninteractive
ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8
RUN echo . \
    && dpkg --add-architecture i386 \
    && apt update && apt upgrade -y \
    && apt install -y --no-install-recommends \
    wget \
    ca-certificates \
    xz-utils \
    libpng-dev \
    zlib1g-dev \
    libtiff-dev \
    libsdl2-dev \
    libsdl2-image-dev \
    graphviz \
    graphviz-dev \
    build-essential \
    libxmu-dev \
    libxi-dev \
    libgl-dev \
    libosmesa-dev \
    python3 \
    python3-pip \
    curl \
    libz1:i386 \
    libc6-dev-i386 \
    libc6:i386 \
    libstdc++6:i386 \
    g++-multilib \
    git \
    diffstat \
    texinfo \
    gawk \
    chrpath \
    libfreetype6-dev \
    mono-runtime \
    flex \
    libssl-dev \
    u-boot-tools \
    libdevil-dev \
    bison \
    python3-pyelftools \
    python3-dev \
    libx11-dev \
    unzip \
    libncurses5 \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /opt
RUN echo . \
    && wget https://software-dl.ti.com/jacinto7/esd/processor-sdk-linux-jacinto7/08_01_00_07/exports/ti-processor-sdk-linux-j7-evm-08_01_00_07-Linux-x86-Install.bin \
    && chmod +x ./ti-processor-sdk-linux-j7-evm-08_01_00_07-Linux-x86-Install.bin \
    && ./ti-processor-sdk-linux-j7-evm-08_01_00_07-Linux-x86-Install.bin \
    && rm /opt/ti-processor-sdk-linux-j7-evm-08_01_00_07-Linux-x86-Install.bin \
    && wget https://software-dl.ti.com/jacinto7/esd/processor-sdk-rtos-jacinto7/08_01_00_13/exports/ti-processor-sdk-rtos-j721e-evm-08_01_00_13.tar.gz \
    && tar xf ti-processor-sdk-rtos-j721e-evm-08_01_00_13.tar.gz \
    && rm /opt/ti-processor-sdk-rtos-j721e-evm-08_01_00_13.tar.gz \
    && cd /opt/ti-processor-sdk-rtos-j721e-evm-08_01_00_13 \
    && ln -s /opt/ti-processor-sdk-linux-j7-evm-08_01_00_07/board-support/prebuilt-images/boot-j7-evm.tar.gz \
    && ln -s /opt/ti-processor-sdk-linux-j7-evm-08_01_00_07/filesystem/tisdk-default-image-j7-evm.tar.xz \
    && cd /opt/ti-processor-sdk-rtos-j721e-evm-08_01_00_13/psdk_rtos \
    && ln -sf $PWD/j7_c_models/lib/PC/x86_64/LINUX/release/libDOF.so /usr/lib/x86_64-linux-gnu/libDOF.so \
    && ln -sf $PWD/j7_c_models/lib/PC/x86_64/LINUX/release/libglbce.so /usr/lib/x86_64-linux-gnu/libApicalSIM.so.1 \
    && cd /opt/ti-processor-sdk-rtos-j721e-evm-08_01_00_13 \
    && /bin/bash ./psdk_rtos/scripts/setup_psdk_rtos.sh --skip_sudo \
    && cd glew-2.0.0 \
    && make install \
    && cd /opt/ti-processor-sdk-rtos-j721e-evm-08_01_00_13/vision_apps \
    && BUILD_EMULATION=no BUILD_TARGET_MODE=yes BUILD_LINUX_A72=yes BUILD_EDGEAI=yes PROFILE=release make all -j2 \
    && BUILD_EMULATION=no BUILD_TARGET_MODE=yes BUILD_LINUX_A72=yes BUILD_EDGEAI=yes PROFILE=release make linux_fs_stage \
    && cd /tmp/tivision_apps_targetfs_stage \
    && tar cJf /opt/tivision_apps.tar.xz . \
    && cd \
    && rm -rf /tmp/tivision_apps_targetfs_stage \
    && cd /opt/ti-processor-sdk-rtos-j721e-evm-08_01_00_13/targetfs \
    && tar cJf /opt/rtos_dlr.tar.xz `find . | grep dlr` \
    && echo .

FROM ubuntu-tisdk1 AS ubuntu-tiros-foxy
ENV DEBIAN_FRONTEND=noninteractive
ENV ROS_DISTRO=foxy

RUN echo . \
    && apt update && apt upgrade -y \
    && apt install -y --no-install-recommends \
    curl gnupg2 lsb-release \
    && curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key  -o /usr/share/keyrings/ros-archive-keyring.gpg \
    && echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu focal main" > /etc/apt/sources.list.d/ros2.list \
    && apt update \
    && apt install -y --no-install-recommends \
    libgstreamer1.0-0 \
    libgstreamer1.0-dev \
    libgstreamer-plugins-base1.0-dev \
    libgstreamer-plugins-good1.0-dev \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-libav \
    gstreamer1.0-tools \
    gir1.2-gst-rtsp-server-1.0 \
    ninja-build \
    python3-pip \
    libdevil-dev libglu1-mesa-dev freeglut3-dev mesa-common-dev \
    wget \
    vim \
    less \
    tmux \
    gdb \
    iputils-ping \
    usbutils \
    ncurses-dev \
    libyaml-cpp-dev \
    rsync \
    strace \
    sysstat \
    gdb \
    net-tools \
    dialog \
    chrony \
    nfs-common \
    corkscrew \
    v4l-utils \
    libeigen3-dev \
    ros-foxy-ros-base \
    ros-foxy-desktop \
    ros-foxy-rmw-cyclonedds-cpp \
    ros-foxy-cv-bridge \
    ros-foxy-image-transport \
    ros-foxy-camera-info-manager \
    ros-foxy-rmw-cyclonedds-cpp \
    ros-foxy-cv-bridge \
    ros-foxy-image-transport \
    ros-foxy-camera-info-manager \
    ros-foxy-rviz2 \
    ros-foxy-image-view \
    ros-foxy-rqt-image-view \
    ros-foxy-rqt-console \
    ros-foxy-rqt-graph \
    libcanberra-gtk-module \
    libcanberra-gtk3-0 \
    libcanberra-gtk3-module \
    python3-opencv \
    python3-colcon-common-extensions \
    python3-colcon-mixin \
    python3-rosdep \
    python3-vcstool \
    && python3 -m pip install --upgrade pip \
    && python3 -m pip install rosbags \
    && python3 -m pip install \
        meson \
        configparser \
        argparse \
    && rm -rf /var/lib/apt/lists/*

RUN rosdep init && \
    rosdep update --rosdistro foxy

WORKDIR /opt
RUN git clone --single-branch --depth 1 --branch REL.08.01.00.03 git://git.ti.com/processor-sdk-vision/jacinto_ros_perception

WORKDIR /opt/jacinto_ros_perception/ros2
RUN git clone --branch ti-ros2 https://github.com/jadonk/gscam
RUN git clone https://github.com/ptrmu/ros2_shared.git
RUN /bin/bash -c "source /opt/ros/foxy/setup.bash && \
    colcon build"
RUN cp /opt/jacinto_ros_perception/docker/entrypoint_x86_64.sh /root/entrypoint.sh
RUN cp /opt/jacinto_ros_perception/docker/ros_setup.sh /root/

WORKDIR /root
RUN echo "if [ -n \"$BASH_VERSION\" ]; then"     >  .profile && \
    echo "    # include .bashrc if it exists"    >> .profile && \
    echo "    if [ -f \"$HOME/.bashrc\" ]; then" >> .profile && \
    echo "        . \"$HOME/.bashrc\""           >> .profile && \
    echo "    fi"                                >> .profile && \
    echo "fi"                                    >> .profile && \
    echo "#!/bin/bash"                           >  .bashrc  && \
    echo "export PS1=\"${debian_chroot:+($debian_chroot)}\u@pc-docker:\w\$ \"" >> .bashrc
RUN echo "#!/bin/sh" >  setup_proxy.sh \
    && echo "" >> setup_proxy.sh \
    && chmod +x setup_proxy.sh \
    && echo .

ARG PROJECT_HOME=j7ros_home
ENV WORK_DIR=/root/${PROJECT_HOME}
ENV ROS_WS=${WORK_DIR}/ros_ws
WORKDIR ${ROS_WS}

ENTRYPOINT ["/root/entrypoint.sh"]


